package com.example.telecompeople_team;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.telecompeople_team.Model.Login;
import com.example.telecompeople_team.Model.Equipe;
import com.example.telecompeople_team.R;
import com.example.telecompeople_team.Rest.JsonParser;
import com.example.telecompeople_team.Rest.RestAddress;
import com.example.telecompeople_team.Utils.FormValidatorUtil;
import com.example.telecompeople_team.Utils.TextWatcherUtil;
import com.example.telecompeople_team.task.HandlerTask;
import com.example.telecompeople_team.task.HandlerTaskAdapter;
import com.example.telecompeople_team.task.TaskRest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;

public class MainActivity extends AppCompatActivity {
    Equipe l;
    public Equipe equipe = new Equipe();

    TextInputEditText editEmail;
    TextInputLayout layoutEmail;
    TextInputEditText editSenha;
    TextInputLayout layoutSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inicializarComponentes();
    }

    public void inicializarComponentes() {
        editEmail = (TextInputEditText) findViewById(R.id.input_email);
        layoutEmail = (TextInputLayout) findViewById(R.id.input_email_layout);
        editSenha = (TextInputEditText) findViewById(R.id.input_senha);
        layoutSenha = (TextInputLayout) findViewById(R.id.input_senha_layout);

        editEmail.addTextChangedListener(new TextWatcherUtil(this, layoutEmail, editEmail));
        editSenha.addTextChangedListener(new TextWatcherUtil(this, layoutSenha, editSenha));
    }

    public void loginOnClick(View view) {

            login();


    }

    private void login() {
        JsonParser<Equipe> jsonParser;
        String json;
        try {


            if (l == null) {
                l = new Equipe();
            }

            l.setLogin(editEmail.getEditableText().toString());
            l.setSenha(editSenha.getEditableText().toString());

            new TaskRest(TaskRest.RequestMethod.GET, handlerLogar).execute(RestAddress.login,l.getLogin(),l.getSenha());


        } catch (RuntimeException e) {
            Toast.makeText(this, "ENTROU AQUI", Toast.LENGTH_SHORT).show();
        }



    }

    private HandlerTask handlerLogar = new HandlerTaskAdapter() {

        @Override
        public void onHandle() {
            super.onHandle();
        }

        @Override
        public void onSuccess(String readValue) {

            super.onSuccess(readValue);
            Toast.makeText(MainActivity.this, ""+readValue, Toast.LENGTH_SHORT).show();

            if(readValue.isEmpty()) {
                Toast.makeText(MainActivity.this, getString(R.string.erro_login), Toast.LENGTH_SHORT).show();
            } else {
                fazLogin(l);
            }



        }

        @Override
        public void onError(Exception erro) {
            super.onError(erro);
            Toast.makeText(MainActivity.this, "pentakill", Toast.LENGTH_SHORT).show();
        }

    };

    private void fazLogin(Equipe equipe) {

        Intent i = new Intent(this, EscolhaAActivity.class);
        i.putExtra("equipe", equipe);
        startActivity(i);
    }



}

