package com.example.telecompeople_team;

import android.content.Intent;
import android.icu.util.LocaleData;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.telecompeople_team.Model.Atividade;
import com.example.telecompeople_team.Model.Equipe;
import com.example.telecompeople_team.Rest.RestAddress;
import com.example.telecompeople_team.task.HandlerTask;
import com.example.telecompeople_team.task.HandlerTaskAdapter;
import com.example.telecompeople_team.task.TaskRest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class EscolhaAActivity extends AppCompatActivity {

    private Spinner spinner;
    private Spinner spinnerSite;
    private Spinner teste;

    List<String> list2;
    List<String> listSite2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_escolha_a);
        inicializaComponentes();
    }

    private void inicializaComponentes(){
        spinner = findViewById(R.id.spinner_atividade);
        spinnerSite = findViewById(R.id.spinner_site);
        teste =  findViewById(R.id.spinner_teste);

        list2 = new ArrayList<>();
        listSite2 = new ArrayList<>();

        addItemsOnSpinnerTeste();
        addItemsOnSpinner();

    }


    public void addItemsOnSpinnerTeste(){
        teste = findViewById(R.id.spinner_teste);
        List<String> list = new ArrayList<String>();
        list.add("list 1");
        list.add("list 2");
        list.add("list 3");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        teste.setAdapter(dataAdapter);
        list.add("4");
        Log.d("Slowly", "lista de vdd"+list.size());
    }


    public void addItemsOnSpinner() {

        List<String> list = new ArrayList<>();
        List<String> listSite = new ArrayList<>();

        new TaskRest(TaskRest.RequestMethod.GET, handlerAtividade).execute(RestAddress.buscaAtividade);
        new TaskRest(TaskRest.RequestMethod.GET, handlerSite).execute(RestAddress.buscaSite);

//
//        while (list2.size() == 0){
//            Log.d("Slowly", "Ainda qui");
//        }

        list = list2;
        listSite = listSite2;
//
//        Log.d("Slowly", ""+list.get(0));
//        Log.d("Slowly", ""+listSite.get(0));

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, list2);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
//
//        spinner.setOnItemSelectedListener(onSpinner());

        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<>(this
                ,android.R.layout.simple_spinner_item, listSite2);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSite.setAdapter(dataAdapter2);

    }

    public List geraLista(){


        return list2;
    }

    public List geraLista2(){



        return listSite2;
    }

    private HandlerTask handlerAtividade = new HandlerTaskAdapter() {

        @Override
        public void onHandle() {
            super.onHandle();
        }

        @Override
        public void onSuccess(String readValue) {

            super.onSuccess(readValue);
            Toast.makeText(EscolhaAActivity.this, ""+readValue, Toast.LENGTH_SHORT).show();

            if(readValue.isEmpty()) {
                Toast.makeText(EscolhaAActivity.this, getString(R.string.erro_login), Toast.LENGTH_SHORT).show();
            } else {
                try {
                    JSONArray jsonArray = new JSONArray(readValue);

                    for (int i = 0; i < jsonArray.length(); i++) {

                        Log.d("teste", "hue");

                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Atividade atividade = new Atividade();
                        atividade.setId(jsonObject.getInt("id"));
                        atividade.setAtividade(jsonObject.getString("atividade"));

                        list2.add(atividade.getAtividade());

                        Log.d("teste", ""+atividade.getAtividade());

                    }
                }catch (JSONException e){

                }



            }



        }

        @Override
        public void onError(Exception erro) {
            super.onError(erro);
            Toast.makeText(EscolhaAActivity.this, "pentakill", Toast.LENGTH_SHORT).show();
        }

    };

    private HandlerTask handlerSite = new HandlerTaskAdapter() {

        @Override
        public void onHandle() {
            super.onHandle();
        }

        @Override
        public void onSuccess(String readValue) {
            super.onSuccess(readValue);
            if(readValue.isEmpty()) {
            } else {
                try {
                    JSONArray jsonArray = new JSONArray(readValue);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String site = jsonObject.getString("site");
                        listSite2.add(site);
                    }



                }catch (JSONException e){
                    Log.d("Slowly", "Catch");
                }
            }
        }
        @Override
        public void onError(Exception erro) {
            super.onError(erro);
            Toast.makeText(EscolhaAActivity.this, "pentakill", Toast.LENGTH_SHORT).show();
        }
    };

    private AdapterView.OnItemSelectedListener onSpinner() {
        Log.d("ass","ass");
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Slowly", "isso");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.d("Slowly", "aquilo");
            }
        };
    }


    public void createDiaria(View view){
        Log.d("teste", "debugar");
       // Intent i = new Intent(this, MainActivity.class);

        Bundle bundle = new Bundle();
        bundle = getIntent().getExtras();
        Log.d("teste", ""+getIntent().getExtras());

        Equipe equipe = (Equipe) bundle.get("equipe");
        Log.d("teste",""+equipe.getSenha());
        Toast.makeText(this, "ggggg", Toast.LENGTH_SHORT).show();

    }






}
