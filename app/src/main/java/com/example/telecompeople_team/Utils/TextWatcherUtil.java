package com.example.telecompeople_team.Utils;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;

import com.example.telecompeople_team.R;


public class TextWatcherUtil implements TextWatcher {

    // Atributos
    TextInputEditText editText;
    TextInputLayout layout;
    Context context;

    // Construtor
    public TextWatcherUtil(Context context, TextInputLayout layout, TextInputEditText editText) {
        this.context = context;
        this.layout = layout;
        this.editText = editText;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        // Verifica o id do EditText
        switch (this.editText.getId()) {
            case R.id.input_email:
                // Valida o email
                // Se não for uma email
                if(!FormValidatorUtil.isValidEmail(editText.getEditableText().toString())) {
                    layout.setError(context.getString(R.string.email_error_msg));
                    editText.requestFocus();
                } else {
                    // Se é um email
                    layout.setErrorEnabled(false);
                }
                break;

            case R.id.input_senha:
                // Se não for uma senha
                if(!FormValidatorUtil.isValidPassword(editText.getEditableText().toString())) {
                    layout.setError(context.getString(R.string.senha_error_msg));
                    editText.requestFocus();
                } else {
                    layout.setEnabled(true);
                }
                break;
        }
    }
}
