package com.example.telecompeople_team.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ReadStreamUtil {

    public static String readStream(InputStream inputStream) {
        try {
            // Faz a leitura do input stream
            BufferedReader leitor = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder builder = new StringBuilder();
            String linha = null;
            // Enquanto houver linhas no buffered reader
            // atribui conteudo a variavel linha
            while ((linha = leitor.readLine()) != null) {
                // Acrescenta a linha ao StringBuilder
                builder.append(linha + "\n");
            }

            // Fecha o buffered header
            leitor.close();
            return builder.toString();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

}
