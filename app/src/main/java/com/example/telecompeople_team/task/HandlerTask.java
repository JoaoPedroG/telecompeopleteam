package com.example.telecompeople_team.task;

public interface HandlerTask {

    void onPreHandler();

    void onHandle();

    void onSuccess(String readValue);

    void onError(Exception erro);
}
