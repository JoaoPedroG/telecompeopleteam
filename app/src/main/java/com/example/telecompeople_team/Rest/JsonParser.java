package com.example.telecompeople_team.Rest;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

public class JsonParser<T> {

    // tipo de objeto
    final Class<T> tipoObjeto;

    // instancia a lib gson
    Gson gson = new Gson();

    // construtor

    public JsonParser(Class<T> tipoObjeto) {
        this.tipoObjeto = tipoObjeto;
    }

    /**
     * Converte um json em uma lista de objetos
     *
     * @param json   json a ser convertido
     * @param objeto objeto produto da conversão
     * @return uma lista de objetos
     */
    public List<T> toList(String json, Class<T[]> objeto) {
        return Arrays.asList(gson.fromJson(json, objeto));
    }

    /**
     * Transforma um json em objeto
     *
     * @param json json a ser convertido
     * @return objeto convertido
     */
    public T toObject(String json) {
        return gson.fromJson(json, tipoObjeto);
    }

    /**
     * Converte um objeto para json
     *
     * @param objeto objeto produto da conversão
     */
    public String fromObject(T objeto) {
        return gson.toJson(objeto);
    }

}
